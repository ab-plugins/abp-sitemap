<?php

/**
 * Automatisation of sitemap
 * Copyright 2019 CrazyCat <crazycat@c-p-f.org>
 */
if (!defined("IN_MYBB")) {
    die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");
}
define('CN_ABPSITEMAP', str_replace('.php', '', basename(__FILE__)));
define('TAB', "\t");

function task_abp_sitemap($task) {
    global $mybb, $lang, $db;
    $lang->load(CN_ABPSITEMAP);
    $mapfile = MYBB_ROOT . 'sitemap.xml';
    if (file_exists($mapfile)) {
        $stats = stat($mapfile);
        if ($stats['mtime'] > (TIME_NOW - (int) $mybb->settings[CN_ABPSITEMAP . '_frequency'] * 3600)) {
            return;
        }
        if (!is_writable($mapfile)) {
            add_task_log($task, $lang->sprintf($lang->abp_sitemap_notwriteable, $mapfile, 'file'));
            return;
        }
    }
    if (!is_writable(MYBB_ROOT)) {
        add_task_log($task, $lang->sprintf($lang->abp_sitemap_notwriteable, MYBB_ROOT, 'directory'));
        return;
    }
    $fo = fopen($mapfile, 'w');
    fwrite($fo, '<?xml version="1.0" encoding="UTF-8"?>' . PHP_EOL);
    fwrite($fo, '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . PHP_EOL);

    $viewableforums = [];
    $forums = $db->simple_select('forums', 'fid', "type='f' AND active=1 AND open=1");
    while ($forum = $db->fetch_array($forums)) {
        $fpermissions = forum_permissions($forum['fid'], 0, $mybb->settings[CN_ABPSITEMAP . '_group']);
        if (!is_array($fpermissions) || (is_array($fpermissions) && array_key_exists('canview', $fpermissions) && (int) $fpermissions['canview'] == 1 && (int) $fpermissions['canview'] == 1 && (int) $fpermissions['canonlyviewownthreads'] == 0)) {
            $viewableforums[] = $forum['fid'];
        }
    }
    if (count($viewableforums) > 0) {
        $threads = $db->simple_select('threads', '*', "visible=1 AND closed NOT LIKE 'moved|%' AND fid IN (" . implode(',', $viewableforums) . ")", ['order_by' => 'tid', 'order_dir' => 'asc']);
        $nbt = 0;
        while ($thread = $db->fetch_array($threads)) {
            if ($mybb->settings[CN_ABPSITEMAP.'_lastpost'] == 1) {
                $lastmod = date('Y-m-d', $thread['lastpost']);
            } else {
                $lastmod = date('Y-m-d', $thread['dateline']);
            }
            fwrite($fo, TAB . '<url>' . PHP_EOL);
            fwrite($fo, TAB . TAB . '<loc>' . $mybb->settings['bburl'] . '/' . get_thread_link($thread['tid']) . '</loc>' . PHP_EOL);
            fwrite($fo, TAB . TAB . '<lastmod>' . $lastmod . '</lastmod>' . PHP_EOL);
            fwrite($fo, TAB . TAB . '<changefreq>' . $mybb->settings[CN_ABPSITEMAP . '_chgfreq'] . '</changefreq>' . PHP_EOL);
            fwrite($fo, TAB . TAB . '<priority>0.8</priority>' . PHP_EOL);
            fwrite($fo, TAB . '</url>' . PHP_EOL);
            $nbt++;
        }
    }
    fwrite($fo, '</urlset>' . PHP_EOL);
    fclose($fo);
    add_task_log($task, $lang->sprintf($lang->abp_sitemap_taskended, $nbt));
}
