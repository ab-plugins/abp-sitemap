<?php

/**
 * Automatisation of sitemap
 * Copyright 2019 CrazyCat <crazycat@c-p-f.org>
 */
if (!defined("IN_MYBB")) {
    die("Direct initialization of this file is not allowed.<br /><br />Please make sure IN_MYBB is defined.");
}
define('CN_ABPSITEMAP', str_replace('.php', '', basename(__FILE__)));

function abp_sitemap_info() {
    global $lang;
    $lang->load(CN_ABPSITEMAP);
    return [
        'name' => $lang->abp_sitemap_name,
        'description' => $lang->abp_sitemap_desc . '<a href=\'https://ko-fi.com/V7V7E5W8\' target=\'_blank\'><img height=\'30\' style=\'border:0px;height:30px;float:right;\' src=\'https://az743702.vo.msecnd.net/cdn/kofi1.png?v=0\' border=\'0\' alt=\'Buy Me a Coffee at ko-fi.com\' /></a>',
        'website' => 'https://gitlab.com/ab-plugins/abp-nsfw',
        'author' => 'CrazyCat',
        'authorsite' => 'http://community.mybb.com/mods.php?action=profile&uid=6880',
        'version' => '0.3',
        'compatibility' => '18*',
        'codename' => CN_ABPSITEMAP
    ];
}

function abp_sitemap_install() {
    global $db, $lang;
    $lang->load(CN_ABPSITEMAP);
     $settinggroups = [
        'name' => CN_ABPSITEMAP,
        'title' => $lang->abp_sitemap_setting_title,
        'description' => $lang->abp_sitemap_setting_desc,
        'disporder' => 0,
        'isdefault' => 0
    ];
    $db->insert_query('settinggroups', $settinggroups);
    $gid = $db->insert_id();
    abp_sitemap_upgrade($gid);
    rebuild_settings();
}

function abp_sitemap_is_installed() {
    global $mybb;
    return(array_key_exists(CN_ABPSITEMAP . '_frequency', $mybb->settings));
}

function abp_sitemap_upgrade($gid=0) {
    global $db, $lang;
    $lang->load(CN_ABPSITEMAP);
    $settings = [
        [
            'name' => CN_ABPSITEMAP . '_frequency',
            'title' => $lang->abp_sitemap_frequency,
            'description' => $lang->abp_sitemap_frequency_desc,
            'optionscode' => 'numeric'.PHP_EOL.'min=1',
            'value' => '1',
            'disporder' => 1
        ],
        [
            'name' => CN_ABPSITEMAP . '_mode',
            'title' => $lang->abp_sitemap_mode,
            'description' => $lang->abp_sitemap_mode_desc,
            'optionscode' => 'select'.PHP_EOL.'0='.$lang->abp_sitemap_mode_task.''.PHP_EOL.'1='.$lang->abp_sitemap_mode_index,
            'value' => '0',
            'disporder' => 2
        ],
        [
            'name' => CN_ABPSITEMAP . '_group',
            'title' => $lang->abp_sitemap_group,
            'description' => $lang->abp_sitemap_group_desc,
            'optionscode' => 'groupselectsingle',
            'value' => '1',
            'disporder' => 3
        ],
        [
            'name' => CN_ABPSITEMAP . '_chgfreq',
            'title' => $lang->abp_sitemap_chgfreq,
            'description' => $lang->abp_sitemap_chgfreq_desc,
            'optionscode' => 'select'.PHP_EOL.'hourly='.$lang->hourly.PHP_EOL.'daily='.$lang->daily.PHP_EOL.'weekly='.$lang->weekly.PHP_EOL.'monthly='.$lang->monthly.PHP_EOL.'yearly='.$lang->yearly,
            'value' => 'daily',
            'disporder' => 3
        ],
        [
            'name' => CN_ABPSITEMAP . '_lastpost',
            'title' => $lang->abp_sitemap_lastpost,
            'description' => $lang->abp_sitemap_lastpost_desc,
            'optionscode' => 'yesno',
            'value' => '1',
            'disporder' => 4
        ],
    ];
    $osettings = [];
    if ((int) $gid == 0) {
        $query = $db->simple_select('settings', 'name, gid', "name LIKE '" . CN_ABPSITEMAP . "%'");
        while ($setted = $db->fetch_array($query)) {
            $osettings[] = $setted['name'];
            $gid = $setted['gid'];
        }
    }
    foreach ($settings as $i => $setting) {
        if (in_array($setting['name'], $osettings)) {
            continue;
        }
        $insert = [
            'name' => $db->escape_string($setting['name']),
            'title' => $db->escape_string($setting['title']),
            'description' => $db->escape_string($setting['description']),
            'optionscode' => $db->escape_string($setting['optionscode']),
            'value' => $db->escape_string($setting['value']),
            'disporder' => $setting['disporder'],
            'gid' => $gid,
        ];
        $db->insert_query('settings', $insert);
    }
}

function abp_sitemap_uninstall() {
    global $db;
    $db->delete_query('settings', "name LIKE '" . CN_ABPSITEMAP . "%'");
    $db->delete_query('settinggroups', "name = '" . CN_ABPSITEMAP . "'");
    rebuild_settings();
}

function abp_sitemap_activate() {
    global $db, $lang;
    $lang->load(CN_ABPSITEMAP);
    abp_sitemap_upgrade();
    require_once MYBB_ROOT . "inc/functions_task.php";
    $task = [
        'title' => $lang->abp_sitemap_task,
        'description' => $lang->abp_sitemap_task_desc,
        'file' => CN_ABPSITEMAP,
        'minute' => '13',
        'hour' => '*',
        'day' => '*',
        'month' => '*',
        'weekday' => '*',
        'enabled' => 1,
        'logging' => 1,
        'locked' => 0
    ];
    $task_insert['nextrun'] = fetch_next_run($task);
    $db->insert_query("tasks", $task);
}

function abp_sitemap_deactivate() {
    global $db;
    $db->delete_query('tasks', "file='" . CN_ABPSITEMAP . "'");
    rebuild_settings();
}

$plugins->add_hook('index_end', 'abp_sitemap_index_end');
function abp_sitemap_index_end() {
    global $mybb, $db;
    if ((int)$mybb->settings[CN_ABPSITEMAP . '_mode'] != 1) {
        return;
    }
    $query = $db->simple_select('tasks', '*', "file='".CN_ABPSITEMAP."'");
    $task = $db->fetch_array($query);
    require_once MYBB_ROOT."inc/functions_task.php";
    run_task($task['tid']);
}