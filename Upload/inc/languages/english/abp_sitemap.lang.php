<?php

/**
 * Automatisation of sitemap
 * Copyright 2019 CrazyCat <crazycat@c-p-f.org>
 */

$l['abp_sitemap_notwriteable'] = 'Cannot write the {2} {1}';
$l['abp_sitemap_taskended'] = 'Sitemap generated with {1} links';