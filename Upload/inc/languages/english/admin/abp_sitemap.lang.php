<?php

/**
 * Automatisation of sitemap
 * Copyright 2019 CrazyCat <crazycat@c-p-f.org>
 */

$l['abp_sitemap_name'] = 'ABP SiteMap';
$l['abp_sitemap_desc'] = 'Automated sitemap generation';

$l['abp_sitemap_setting_title'] = 'ABP Sitemap settings';
$l['abp_sitemap_setting_desc'] = 'Various settings for ABP sitemap';
$l['abp_sitemap_frequency'] = 'Update frequency';
$l['abp_sitemap_frequency_desc'] = 'Frequency (in hours) for the refresh of the sitemap';
$l['abp_sitemap_mode'] = 'Refresh mode';
$l['abp_sitemap_mode_desc'] = 'Refresh using the task manager or when index page is loaded (deprecated) ?<br />Remember that the task have to be enabled whatever your choice is.';
$l['abp_sitemap_mode_task'] = 'Task manager';
$l['abp_sitemap_mode_index'] = 'Index page';
$l['abp_sitemap_group'] = 'User group';
$l['abp_sitemap_group_desc'] = 'The user group permissions applied for the sitemap (read permission) -  Note: It is not recommended you change this from the default Guests group';
$l['abp_sitemap_chgfreq'] = 'Change frequency';
$l['abp_sitemap_chgfreq_desc'] = 'How frequently the pages are likely to change (indication for crawlers)';
$l['abp_sitemap_lastpost'] = 'Last post date';
$l['abp_sitemap_lastpost_desc'] = 'Use the date of last post of a thread as last modification time ?';

$l['hourly'] = 'hourly';
$l['daily'] = 'daily';
$l['weekly'] = 'weekly';
$l['monthly'] = 'monthly';
$l['yearly'] = 'yearly';

$l['abp_sitemap_task'] = 'ABP SiteMap generation';
$l['abp_sitemap_task_desc'] = 'Generates the sitemap for the board';
$l['abp_sitemap_notwriteable'] = 'Cannot write the {2} {1}';
$l['abp_sitemap_taskended'] = 'Sitemap generated with {1} links';