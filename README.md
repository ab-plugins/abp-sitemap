# ABP Sitemap

Automated sitemap generation

Adds a task to automate the sitemap generation of your forum

# Settings
* frequency : the frequency of the update
* mode : how is updated the sitemap ? Allowed choice are task and index
  * task (default) : the task settings are used
  * index (deprecated) : the task runs each time the index page is loaded
* group : the user group permissions applied for the sitemap
* change frequency : indication for crawlers
* last post date : using the last post date as last modificate date

# Installation / upgrade
* Installation : Upload all files to your forum root directory and install from ACP.
* upgrade : upload all files to your forum root directory, deactivate and reactivate the plugin

# Notice
Think to add your sitemap url in your robots.txt: `Sitemap: https://your.board.url/sitemap.xml`